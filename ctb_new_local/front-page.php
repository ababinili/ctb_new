<?php
/**
 * The template for displaying main pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ctb_new
 */

get_header();
?>
<body>
    <div class="w-100 banner-paralax">

        <div id="target" class="parallax-port d-none d-md-block">
            <img class="parallax-layer" src="<?php bloginfo("template_directory");?>/images/target_blue4.png"
                 alt="ПОИСК ТУРА"/>
            <img class="parallax-layer paralax-text img-fluidg"
                 src="<?php bloginfo("template_directory");?>/images/target_green4.png" alt="ПОИСК ТУРА"/>
            <img class="parallax-layer" src="<?php bloginfo("template_directory");?>/images/target_red4.png"
                 alt="ПОИСК ТУРА"/>
        </div>

        <div class="d-flex justify-content-around align-items-center h-765 position-absolute w-100" id="top-1">
            <div>
                <a href="">
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/ico-1.png" alt="">
                    <span>Горящие</span>
                </a>
            </div>
            <div>
                <a href="">
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/ico-2.png" alt="">
                    <span>Раннее Бронирование</span>
                </a>
            </div>
            <div>
                <a href="">
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/ico-3.png" alt="">
                    <span>Заказать трансфер</span>
                </a>
            </div>
        </div>

    </div>
    <div class="w-100 d-none d-sm-block">
        <div class="first-carousel">
            <div>
                <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/slide-1.png" alt="">
            </div>
            <div>
                <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/slide-2.png" alt="">
            </div>
            <div>
                <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/slide-3.png" alt="">
            </div>
            <div>
                <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/slide-4.png" alt="">
            </div>
        </div>
    </div>
    <div class="w-100 d-sm-none">
        <div class="first-carousel-small">
            <div>
                <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/slide-1-small.png" alt="">
            </div>
            <div>
                <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/slide-2-small.png" alt="">
            </div>
            <div>
                <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/slide-3-small.png" alt="">
            </div>
            <div>
                <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/slide-4-small.png" alt="">
            </div>
        </div>
    </div>
    <div class="w-100 front-page-background d-custom-md-none">
        <div class="container">
            <div class="row">
                <div class="col d-flex flex-column align-items-end justify-content-center h-47r">
                    <div>
                        <span class="font-weight-very-bold text-white line-height-1 font-size-4_7">ЗАКАЖИ <br> ПОДБОР <br> ТУРА</span>
                        <div class="mt-3">
                            <button class="text-white btn button-red-style py-3 px-5 font-weight-normal">ЗАКАЗАТЬ</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="w-100 front-page-background-md d-custom-md-visible">
        <div class="container">
            <div class="row">
                <div class="col d-flex justify-content-center">
                    <div class="d-flex flex-column align-items-center text-center">
                        <span class="font-weight-very-bold text-white line-height-1 font-size-4_7">ЗАКАЖИ ПОДБОР ТУРА</span>
                        <div>
                            <button class="mt-3 mt-sm-5 text-white btn button-red-style py-3 px-5 font-weight-normal">
                                ЗАКАЗАТЬ
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col text-center my-3">
                <h2 class="text-grapes font-weight-very-bold">ПАРТНЁРЫ</h2>
            </div>
        </div>
        <div class="row mb-0 mb-sm-5 mb-lg-0 ">
            <div class="col mb-3 d-flex align-items-center justify-content-center flex-md-column">
                <div class="d-flex d-md-block flex-column">
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/partners-anex.png" alt="">
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/partners-biblio.png" alt="">
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/partners-pegas.png" alt="">
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/Alean.png" alt="">
                    <img class="img-fluid d-none d-lg-inline" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_VIking.png" alt="">
                </div>
                <div class="col d-none d-md-block d-lg-none">
                    <div>
                        <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/partners-anex.png" alt="">
                        <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/partners-biblio.png" alt="">
                        <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/partners-pegas.png" alt="">
                        <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/Alean.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="row pt-0 pt-md-5 pb-4">
            <div class="col text-center">
                <button class="btn button-empty-style text-grapes py-3 px-5 font-weight-very-bold">
                    ВСЕ ПАРТНЁРЫ
                </button>
            </div>
        </div>
    </div>
    <div class="w-100 black-background-block ">
        <div class="container">
            <div class="row pt-5 pb-2">
                <div class="col text-center">
                    <h3 class="text-white font-weight-bold">НАША КОМАНДА</h3>
                </div>
            </div>
            <div class="row pt-3 d-none d-md-block">
                <div class="col d-flex">
                    <div class="thumbnail">
                        <img src="<?php bloginfo("template_directory");?>/images/team/Popova-logo.jpg" alt="">
                        <div class="caption d-flex flex-column">
                            <span class="pt-4 pb-3">Юлия Попова</span>
                            <span>Генеральный директор</span>
                        </div>
                    </div>
                    <div class="thumbnail">
                        <img src="<?php bloginfo("template_directory");?>/images/team/Popova-logo.jpg" alt="">
                        <div class="caption d-flex flex-column">
                            <span class="pt-4 pb-3">Юлия Попова</span>
                            <span>Генеральный директор</span>
                        </div>
                    </div>
                    <div class="thumbnail">
                        <img src="<?php bloginfo("template_directory");?>/images/team/Popova-logo.jpg" alt="">
                        <div class="caption d-flex flex-column">
                            <span class="pt-4 pb-3">Юлия Попова</span>
                            <span>Генеральный директор</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row d-none d-md-block">
                <div class="col d-flex justify-content-center">
                    <div class="thumbnail">
                        <img src="<?php bloginfo("template_directory");?>/images/team/Popova-logo.jpg" alt="">
                        <div class="caption d-flex flex-column">
                            <span class="pt-4 pb-3">Юлия Попова</span>
                            <span>Генеральный директор</span>
                        </div>
                    </div>
                    <div class="thumbnail">
                        <img src="<?php bloginfo("template_directory");?>/images/team/Popova-logo.jpg" alt="">
                        <div class="caption d-flex flex-column">
                            <span class="pt-4 pb-3">Юлия Попова</span>
                            <span>Генеральный директор</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row d-md-none">
                <div class="col">
                    <div class="second-carousel">
                        <div>
                            <div class="thumbnail">
                                <img src="<?php bloginfo("template_directory");?>/images/team/Popova-logo.jpg" alt="">
                                <div class="caption d-flex flex-column">
                                    <span class="pt-4 pb-3">Юлия Попова</span>
                                    <span>Генеральный директор</span>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="thumbnail">
                                <img src="<?php bloginfo("template_directory");?>/images/team/Popova-logo.jpg" alt="">
                                <div class="caption d-flex flex-column">
                                    <span class="pt-4 pb-3">Юлия Попова</span>
                                    <span>Генеральный директор</span>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="thumbnail">
                                <img src="<?php bloginfo("template_directory");?>/images/team/Popova-logo.jpg" alt="">
                                <div class="caption d-flex flex-column">
                                    <span class="pt-4 pb-3">Юлия Попова</span>
                                    <span>Генеральный директор</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="w-100">
        <div class="container">
            <div class="row my-5">
                <div class="col third-carousel">
                    <div class="d-flex flex-column justify-content-center flex-md-row">
                        <div class="col-12 col-md-5 ">
                            <div class="d-flex flex-column align-items-center">
                                <div>
                                    <img class=" img-fluid" src="<?php bloginfo("template_directory");?>/images/turciy-img.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-1 my-3 my-md-0">
                            <div class="d-flex justify-content-center">
                                <img src="<?php bloginfo("template_directory");?>/images/cc.png" alt="">
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="d-flex flex-column">
                                <span class="">Отдыхали в Турции ( Кемер ), туроператор TUI. Жили в отеле Asdem park 4*. В отеле не первый раз. Очень удобное расположение – практически центр города. Вторая линия, есть собственный пляж. Отель очень молодежный, шумный. Дискотеки и шоу программа каждый вечер. Есть второй корпус- более тихий. Гости отеля – русские, турки. Питание соответствует 4-м звёздам отеля. В меру чисто, техника исправна. Была проблема с кондиционером, исправили по вызову очень быстро. Рекомендуем для весёлого, шумного отдыха в дружной компании!</span>
                                <span class="mt-5 font-weight-bold">НАТАЛЬЯ ВИШНЕВСКАЯ</span>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-column justify-content-center flex-md-row">
                        <div class="col-12 col-md-5 ">
                            <div class="d-flex flex-column align-items-center">
                                <div>
                                    <img class=" img-fluid" src="<?php bloginfo("template_directory");?>/images/turciy-img.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-1 my-3 my-md-0">
                            <div class="d-flex justify-content-center">
                                <img src="<?php bloginfo("template_directory");?>/images/cc.png" alt="">
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="d-flex flex-column">
                                <span class="">Отдыхали в Турции ( Кемер ), туроператор TUI. Жили в отеле Asdem park 4*. В отеле не первый раз. Очень удобное расположение – практически центр города. Вторая линия, есть собственный пляж. Отель очень молодежный, шумный. Дискотеки и шоу программа каждый вечер. Есть второй корпус- более тихий. Гости отеля – русские, турки. Питание соответствует 4-м звёздам отеля. В меру чисто, техника исправна. Была проблема с кондиционером, исправили по вызову очень быстро. Рекомендуем для весёлого, шумного отдыха в дружной компании!</span>
                                <span class="mt-5 font-weight-bold">НАТАЛЬЯ ВИШНЕВСКАЯ</span>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-column justify-content-center flex-md-row">
                        <div class="col-12 col-md-5 ">
                            <div class="d-flex flex-column align-items-center">
                                <div>
                                    <img class=" img-fluid" src="<?php bloginfo("template_directory");?>/images/turciy-img.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-1 my-3 my-md-0">
                            <div class="d-flex justify-content-center">
                                <img src="<?php bloginfo("template_directory");?>/images/cc.png" alt="">
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="d-flex flex-column">
                                <span class="">Отдыхали в Турции ( Кемер ), туроператор TUI. Жили в отеле Asdem park 4*. В отеле не первый раз. Очень удобное расположение – практически центр города. Вторая линия, есть собственный пляж. Отель очень молодежный, шумный. Дискотеки и шоу программа каждый вечер. Есть второй корпус- более тихий. Гости отеля – русские, турки. Питание соответствует 4-м звёздам отеля. В меру чисто, техника исправна. Была проблема с кондиционером, исправили по вызову очень быстро. Рекомендуем для весёлого, шумного отдыха в дружной компании!</span>
                                <span class="mt-5 font-weight-bold">НАТАЛЬЯ ВИШНЕВСКАЯ</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php wp_footer(); ?>
</body>

<?php
get_footer();
