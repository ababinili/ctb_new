<?php
/**
 * The header for page
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ctb_new
 */
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head(); ?>
    <meta name="KeyWords"
          content="Центр Туристического Бронирования ярославль, ЦТБ, горящие путевки, ЦТБ поиск туров, горящие путевки, горящие путевки Ярославль, горящие туры, подбор тура, Раннее Бронирование, сайт поиска тура">

</head>
<header>
    <div class="w-100 header-background-transparent">
        <div class="container-fluid">
            <div class="row  d-flex justify-content-around">
                <div class="col-3 my-2">
                    <img class="img-fluid header-logo-wight  float-right" src="<?php bloginfo("template_directory");?>/images/logo.png" alt="">
                </div>
                <div class="col-7 align-items-center justify-content-evenly d-none d-sm-flex">
                    <div class="d-flex align-items-center">
                        <a href="">
                            <img class="mx-2" src="<?php bloginfo("template_directory");?>/images/social-logo/vk.svg" alt="">
                        </a>
                        <a href="">
                            <img class="mx-2" src="<?php bloginfo("template_directory");?>/images/social-logo/instagram.svg" alt="">
                        </a>
                        <a href="">
                            <img class="mx-2" src="<?php bloginfo("template_directory");?>/images/social-logo/facebook.svg" alt="">
                        </a>
                        <a href="">
                            <img class="mx-2" src="<?php bloginfo("template_directory");?>/images/social-logo/odnoklassniki-logo.svg" alt="">
                        </a>

                        <a href="tel:+7-4852-94-14-50"
                           class="text-white-no_impotant text-hover-gold font-size-1_5 mx-2 d-none d-md-block"
                           role="button" title="Телефон ЦТБ">94-14-50</a>
                    </div>
                    <button class="text-white btn button-red-style d-none d-lg-block">Подобрать тур</button>
                </div>
                <div class="col-2 d-flex justify-content-center align-items-center">
                    <a href="">
                        <div class="lk-box-img mr-3"></div>
                    </a>
                    <a id="trigger-overlay" >
                        <span class="text-white-no_impotant text-hover-gold hidden-1010">Меню</span>
                        <img src="<?php bloginfo("template_directory");?>/images/burger-old.png" alt="">
                    </a>
                    <!-- open/close -->
                    <div class="overlay overlay-slidedown">

                        <button type="button" class="overlay-close">Close</button>
                        <a type="button" href="<? echo get_home_url() ?>" class="overlay-home">Home</a>
                        <nav><?php wp_nav_menu($Menu_header); ?></nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
