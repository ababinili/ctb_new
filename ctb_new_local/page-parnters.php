<?php
/*
Template Name: Страница Партнёры
*/
get_header('page');
?>
<div class="w-100 banner-partners-background">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="title-banner-block d-flex justify-content-center flex-column">
                    <h1 class="text-white font-weight-bold">ПАРТНЁРЫ</h1>
                    <span class="text-gold my-4 font-weight-bold">РАБОТАЕМ ТОЛЬКО С ПРОВЕРЕННЫМИ ТУРОПЕРАТОРАМИ</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container text-align-center">
    <div class="row ">
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
    </div>
</div>
<?php
wp_footer();
get_footer('page');
?>