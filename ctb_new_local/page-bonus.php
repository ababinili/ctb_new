<?php
/*
Template Name: Страница Программа привилегий
*/
get_header('page');
?>
<div class="w-auto banner-bonus-background">
    <div class="container">
        <div class="row">
            <div class="col-8">
                <div class="title-banner-block d-flex justify-content-center flex-column">
                    <h1 class="text-white font-weight-bold">ПРОГРАММА ПРИВИЛЕГИЙ "ПЯТЬ ЗВЕЗД"</h1>
                    <span class="text-gold my-4 ">БОЛЬШЕ ПУТЕШЕСТВИЙ - БОЛЬШЕ ПРИВИЛЕГИЙ</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col d-flex align-items-center py-5 flex-column">
            <h2 class=" font-weight-bold text-gold">ВЫБЕРИ СВОЙ ВАРИАНТ</h2>
            <span class="text-center">Наша программа привилегий включает 5 статусов, в зависимости
от количества и частоты путешествий.</span>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/card-2.png" alt="">
        </div>
    </div>
</div>
<div class="w-100 banner-bonus-two-background">
    <div class="container text-gold font-weight-very-bold">
        <div class="row">
            <div class="col d-flex justify-content-center my-5">
                <h3 class="">КАК ПОТРАТИТЬ БОНУСЫ?</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3 d-flex flex-column align-items-center">
                <div>
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/rger.png" alt="">
                </div>
                <span class="my-5">СУВЕНИРЫ</span>
            </div>
            <div class="col-12 col-sm-3 d-flex d-flex flex-column align-items-center">
                <div>
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/ewetpng.png" alt="">
                </div>
                <span class="my-5">СКИДКИ НА ТУР</span>
            </div>
            <div class="col-12 col-sm-3 d-flex d-flex flex-column align-items-center">
                <div>
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/12132t.png" alt="">
                </div>
                <span class="my-5">ПОДАРОЧНЫЕ <br> СЕРТИФИКАТЫ</span>
            </div>
            <div class="col-12 col-sm-3 d-flex d-flex flex-column align-items-center">
                <div>
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/gewe.png" alt="">
                </div>
                <span class="my-5">ТРАНСФЕРЫ</span>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-4 d-flex d-flex flex-column align-items-center">
                <div>
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/sgsrgh.png" alt="">
                </div>
                <span class="my-5">ПОДАРКИ ОТ ПАРТНЕРОВ</span>
            </div>
            <div class="col-12 col-sm-4 d-flex d-flex flex-column align-items-center text-center">
                <div>
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/drhedrh.png" alt="">
                </div>
                <span class="my-5">ЗАКРЫТЫЕ <br> МЕРОПРИЯТИЯ <br> ДЛЯ КЛИЕНТОВ</span>
            </div>
            <div class="col-12 col-sm-4 d-flex d-flex flex-column align-items-center">
                <div>
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/rgeh.png" alt="">
                </div>
                <span class="my-5">СТРАХОВАНИЕ</span>
            </div>
        </div>
        <div class="row">
            <div class="col d-flex flex-column align-items-center">
                <button class="btn button-red-style text-white">ПОЛУЧИТЬ КАРТУ</button>
                <button class="btn button-red-style text-white my-3">ПОДРОБНЕЕ О ПРОГРАММЕ</button>
            </div>
        </div>
    </div>
</div>
<?php
wp_footer();
get_footer('page');
?>
