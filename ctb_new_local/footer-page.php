<?php
/**
 * The template for displaying the footer in page
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ctb_new
 */
?>
</body>
<footer>
    <div class="w-100 footer-page-background">
        <div class="container">
            <div class="row">
                <div class="col d-md-flex mb-0 my-md-5">
                    <span class="text-white font-weight-very-bold footer-big-tur-word line-height-1">БОЛЬШЕ ЧЕМ<br> ТУРАГЕНСТВО</span>
                    <div class="d-md-flex flex-column col my-4 my-md-0">
                        <div class="d-md-flex justify-content-around">
                    <span class="text-white-no_impotant">
                    <ul class="pl-0 mb-0 mb-md-3">
                        <li><a href="" class="footer-menu-decorate">Фото и Видео</a></li>
                        <li><a href="" class="footer-menu-decorate">Поиск тура</a></li>
                        <li><a href="" class="footer-menu-decorate">Горящие</a></li>
                        <li><a href="" class="footer-menu-decorate">Раннее Бронирование</a></li>
                    </ul>
                    </span>

                            <span class="text-white-no_impotant">
                    <ul class="pl-0">
                        <li><a href="" class="footer-menu-decorate">Акции и Бонусы</a></li>
                        <li><a href="" class="footer-menu-decorate">Отзывы</a></li>
                        <li><a href="" class="footer-menu-decorate">Услуги</a></li>
                        <li><a href="" class="footer-menu-decorate">Программа привилегий</a></li>
                    </ul>
                </span>
                        </div>

                        <div class="d-flex justify-content-center">
                            <span class="text-gold">ООО "ЦЕНТР ТУРИСТИЧЕСКОГО БРОНИРОВАНИЯ"</span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</footer>
</html>