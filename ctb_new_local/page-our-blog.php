<?php
/*
Template Name: Страница Наш блог
*/
get_header('page');
?>
    <div class="w-100 banner-our-blog-background">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="title-banner-block d-flex justify-content-center flex-column">
                        <h1 class="text-white font-weight-bold">НАШ БЛОГ</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <?php query_posts('cat=5&order=ASC'); ?>

        <?php if (have_posts()) : ?>

            <?php while (have_posts()) : the_post(); ?>
                <div class="row py-4">
                    <div class="col">
                        <a href="<? the_permalink(); ?>" class="blog-title-color">
                            <span class="font-size-36 font-weight-very-bold">
                                <?php the_title(); ?>
                            </span>
                        </a>
                        <span>
                            <?php the_excerpt(); ?>
                        </span>
                        <div>
                            <button href="<? the_permalink(); ?>" class="text-white btn button-red-style d-none d-lg-block">
                                Читать далее...
                            </button>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>

        <?php else : ?>

            <h2>Записей нет</h2>

        <?php endif; ?>
    </div>
<?php
wp_footer();
get_footer('page');
?>