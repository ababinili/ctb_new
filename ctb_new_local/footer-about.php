<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ctb_new
 */

?>
</body>
<footer>
    <div class="w-100 footer-about-background">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6 d-flex flex-column">
                    <span class="text-gold font-weight-very-bold mt-4 font-size-50">КОНТАКТЫ</span>
                    <span class="text-white font-weight-very-bold mb-4">ООО "ЦЕНТР ТУРИСТИЧЕСКОГО БРОНИРОВАНИЯ"</span>

                    <div class="d-flex flex-column my-4 font-size-18">
                        <span class="text-gold font-weight-bold">АДРЕС</span>
                        <span class="text-white">г. Ярославль, ул. Победы, д. 43/61</span>
                        <span class="text-white">ТЦ "МИГ" (у Макдональдса), 2-ой этаж</span>
                    </div>
                    <div class="d-flex flex-column my-3 font-size-18">
                        <span class="text-gold font-weight-bold">ГРАФИК РАБОТЫ</span>
                        <span class="text-white">info@ctb76.ru</span>
                    </div>
                    <div class="d-flex flex-column my-3 font-size-18">
                        <span class="text-gold font-weight-bold">E-MAIL</span>
                        <span class="text-white">info@ctb76.ru</span>
                    </div>
                    <div class="d-flex flex-column my-4 font-size-18">
                        <span class="text-gold font-weight-bold">ТЕЛЕФОН:</span>
                        <span class="text-white">+7 (4852) 94-14-50, +7 (909) 280-90-55, +7 (920) 118-20-57</span>
                    </div>
                    <div class="d-flex flex-column my-4 font-size-18">
                        <span class="text-gold font-weight-bold">WHATSAPP, VIBER:</span>
                        <span class="text-white">+7 (909) 280-90-55</span>
                    </div>
                    <div class="d-flex flex-column font-size-18 d-sm-none my-4">
                        <span class="text-gold font-weight-bold">СОЦИАЛЬНЫЕ СЕТИ</span>
                        <div class="mt-2">
                            <a href="">
                                <img class="mx-2"
                                     src="<?php bloginfo("template_directory"); ?>/images/social-logo/vk.svg" alt="">
                            </a>
                            <a href="">
                                <img class="mx-2"
                                     src="<?php bloginfo("template_directory"); ?>/images/social-logo/instagram.svg"
                                     alt="">
                            </a>
                            <a href="">
                                <img class="mx-2"
                                     src="<?php bloginfo("template_directory"); ?>/images/social-logo/facebook.svg"
                                     alt="">
                            </a>
                            <a href="">
                                <img class="mx-2"
                                     src="<?php bloginfo("template_directory"); ?>/images/social-logo/odnoklassniki-logo.svg"
                                     alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-6 d-none d-md-flex align-items-center">
                    <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A2fe6cc19d78323989fbaeafd0a3d35f5724d264e322698483080d3bcebee9733&amp;source=constructor" width="100%" height="455" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="w-100 footer-page-background">
        <div class="container">
            <div class="row">
                <div class="col d-md-flex mb-0 my-md-5">
                    <span class="text-white font-weight-very-bold footer-big-tur-word line-height-1">БОЛЬШЕ ЧЕМ<br> ТУРАГЕНСТВО</span>
                    <div class="d-md-flex flex-column col my-4 my-md-0">
                        <div class="d-md-flex justify-content-around">
                    <span class="text-white-no_impotant">
                    <ul class="pl-0 mb-0 mb-md-3">
                        <li><a href="" class="footer-menu-decorate">Фото и Видео</a></li>
                        <li><a href="" class="footer-menu-decorate">Поиск тура</a></li>
                        <li><a href="" class="footer-menu-decorate">Горящие</a></li>
                        <li><a href="" class="footer-menu-decorate">Раннее Бронирование</a></li>
                    </ul>
                    </span>

                            <span class="text-white-no_impotant">
                    <ul class="pl-0">
                        <li><a href="" class="footer-menu-decorate">Акции и Бонусы</a></li>
                        <li><a href="" class="footer-menu-decorate">Отзывы</a></li>
                        <li><a href="" class="footer-menu-decorate">Услуги</a></li>
                        <li><a href="" class="footer-menu-decorate">Программа привилегий</a></li>
                    </ul>
                </span>
                        </div>

                        <div class="d-flex justify-content-center">
                            <span class="text-gold">ООО "ЦЕНТР ТУРИСТИЧЕСКОГО БРОНИРОВАНИЯ"</span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</footer>
</html>
