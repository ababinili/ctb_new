<?php
/*
Template Name: Страница Фото и Видео
*/
get_header('page');
?>
<div class="w-100d banner-photo-background d-flex align-items-center justify-content-center">
    <div class="container">
        <div class="row">
            <div class="col-sm">
                <div class="">
                    <h1 class="font-weight-bold font-size-5 text-white">ФОТО<br>
                        И ВИДЕО
                    </h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="w100 black-background-block">
    <div class="container position-relative container-imposing">
        <div class="row d-flex justify-content-center">
            <div class="col-sm col-xs-12 pl-0 pr-0 position-relative w-auto">
                <img src="<?php bloginfo("template_directory");?>/images/222333.png" class="img-fluid" alt="">
                <div class="position-absolute fixed-bottom title-thumbnail-box  d-flex align-items-center">
                    <span class="font-weight-bold text-white font-size-1_9 ml-2">НАШИ ПОЕЗДКИ</span>
                </div>
            </div>
            <div class="col-sm col-xs-12 pl-0 pr-0 position-relative w-auto">
                <img src="<?php bloginfo("template_directory");?>/images/222333.png" class="img-fluid" alt="">
                <div class="position-absolute fixed-bottom title-thumbnail-box  d-flex align-items-center">
                    <span class="font-weight-bold text-white font-size-1_9 ml-2">НАШИ ПОЕЗДКИ</span>
                </div>
            </div>
            <div class="col-sm col-xs-12 pl-0 pr-0 position-relative w-auto">
                <img src="<?php bloginfo("template_directory");?>/images/222333.png" class="img-fluid" alt="">
                <div class="position-absolute fixed-bottom title-thumbnail-box  d-flex align-items-center">
                    <span class="font-weight-bold text-white font-size-1_9 ml-2">НАШИ ПОЕЗДКИ</span>
                </div>
            </div>
        </div>
        <div class="row d-flex justify-content-center">
            <div class="col-sm col-xs-12 pl-0 pr-0 position-relative w-auto">
                <img src="<?php bloginfo("template_directory");?>/images/222333.png" class="img-fluid" alt="">
                <div class="position-absolute fixed-bottom title-thumbnail-box  d-flex align-items-center">
                    <span class="font-weight-bold text-white font-size-1_9 ml-2">НАШИ ПОЕЗДКИ</span>
                </div>
            </div>
            <div class="col-sm col-xs-12 pl-0 pr-0 position-relative w-auto">
                <img src="<?php bloginfo("template_directory");?>/images/222333.png" class="img-fluid" alt="">
                <div class="position-absolute fixed-bottom title-thumbnail-box  d-flex align-items-center">
                    <span class="font-weight-bold text-white font-size-1_9 ml-2">НАШИ ПОЕЗДКИ</span>
                </div>
            </div>
            <div class="col-sm pl-0 pr-0 position-relative w-auto">
                <img src="<?php bloginfo("template_directory");?>/images/222333.png" class="img-fluid" alt="">
                <div class="position-absolute fixed-bottom title-thumbnail-box  d-flex align-items-center">
                    <span class="font-weight-bold text-white font-size-1_9 ml-2">НАШИ ПОЕЗДКИ</span>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
wp_footer();
get_footer('page');
?>