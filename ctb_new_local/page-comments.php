<?php
/*
Template Name: Страница Отзывы
*/
get_header('page'); ?>

    <div class="w-auto banner-comments-background">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <div class="title-banner-block d-flex justify-content-center flex-column">
                        <h1 class="text-white font-weight-bold">ОТЗЫВЫ</h1>
                        <span class="text-white my-4 ">Мнение каждого туриста очень важно и ценно для нас Здесь вы найдете отзывы наших постоянных туристов и сможете поделиться своими впечатлениями об отдыхе и работе наших специалистов.</span>
                        <div>
                            <button type="button" class="text-white btn button-red-style">ОСТАВИТЬ ОТЗЫВ</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <?php query_posts('cat=7&order=ASC'); ?>
        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
                <div class="row py-4">
                    <div class="col-12 col-md-3 d-flex justify-content-center">
                        <div class="d-flex flex-column">
                            <?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?>
                            <span class="mt-3 font-size-13 font-weight-bold"><?php the_title(); ?></span>
                        </div>
                    </div>
                    <div class="col-12 col-md-1 my-3 my-md-0 d-flex justify-content-center">
                        <div>
                            <img src="<?php bloginfo("template_directory"); ?>/images/cc.png" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-md-8 d-flex flex-column mt-0">
                        <span class="font-weight-bold"><?php the_content(); ?></span>
                        <span class="mt-5 font-weight-bold"><?php echo(get_post_meta($post->ID, 'name', true)); ?></span>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php else : ?>
            <h2>Записей нет</h2>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
        <?php wp_footer(); ?>
    </div>
<?php
wp_footer();
get_footer('page');
?>