<?php
/*
Template Name: Страница Акции
*/
get_header('page'); ?>
<div class="w-100 banner-event-background">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="title-banner-block d-flex align-items-center">
                    <h1 class="text-white font-weight-bold">АКЦИИ <br> И БОНУСЫ</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="w-100 black-background-block">
    <div class="container container-imposing position-relative">
        <div class="row">
            <div class="col event-text-block">
                <span><?php while (have_posts()) : the_post(); ?>
                        <div class="entry-content">
                                <?php the_content(); ?>
                            </div><!-- .entry-content -->
                            <?php comments_template('', true); ?>
                        <?php endwhile; // end of the loop. ?></span>
                <button type="button" class="text-white btn button-red-style mt-5">ПОЛУЧИТЬ РАССЫЛКУ О СКИДКАХ И АКЦИЯХ
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="col d-flex justify-content-center py-5">
                    <h2 class="font-weight-bold text-gold">Наши предложения</h2>
                </div>
            </div>
        </div>
<?php query_posts('cat=2&order=ASC'); ?>
<?php if(have_posts()) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="row py-3 ">
            <div class="col-sm-12 col-lg-5 d-flex justify-content-center">
                <div id="post-<?php the_id(); ?>" <?php post_class(); ?>
                <div>
                    <img class=" img-fluid" src="<?php the_post_thumbnail(); ?>" alt="">
                </div>
            </div>
            <div class="col-sm-12 col-lg-7 d-flex flex-column mt-0 mt-lg-4">
                <div class="d-flex justify-content-start">
                    <div class="text-white mb-3 event-date-background">
                        <?php echo (get_post_meta($post->ID, 'date', true)); ?>
                    </div>
                </div>
                <span class=" text-white font-weight-bold"><?php the_title();  ?></span>
                <span class="pt-3 text-white"><?php the_content(); ?></span>
            </div>
        </div>
    <?php endwhile; ?>
<?php else : ?>
    <h2>Записей нет</h2>
<?php endif; ?>
        <?php wp_reset_query(); ?>
        <?php wp_footer(); ?>
    </div>
</div>
<?php
wp_footer();
get_footer('page');
?>