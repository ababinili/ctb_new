$(document).ready(function () {
    $('.first-carousel').slick({
        arrow: false,
        prevArrow: '<div class="slick-prev" aria-label="Previous"  style="">' +
        '<img class="img-fluid" src="images/arro-left.png" alt="">' +
        '</div>',
        nextArrow: '<div class="slick-next" aria-label="Next"  style="">' +
        '<img class="img-fluid" src="images/arro-right.png" alt="">' +
        '</div>'
    });
    $('.first-carousel-small').slick({
        dots: true,
        arrows: false
    });

    $('.second-carousel').slick({
        dots: true,
        arrows: false
    });

    $('.third-carousel').slick({
        dots: true,
        arrows: true,
        prevArrow: '<button class="slick-prev slick-arrow d-none d-md-block" aria-label="Previous" type="button" style="">Previous</button>',
        nextArrow: '<button class="slick-next slick-arrow d-none d-md-block" aria-label="Next" type="button" style="">Next</button>'
    });

});
